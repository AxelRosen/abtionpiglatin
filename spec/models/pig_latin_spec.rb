require 'rails_helper'

RSpec.describe PigLatin, type: :model do
  it "rearranges consonants to the ending and add ay" do
    expect(PigLatin.new.constants("happy")).to eq("appyhay")
  end

  it "rearranges two consonants to the ending and add ay" do
    expect PigLatin.new.twoConstants("child").to eq("ildchay")
  end

end
